This is an example of a python lambda service.


deployed at:
https://aapjeisbaas.nl/api/v1/fibonacci?n=20


Corresponding blog post:
https://aapjeisbaas.nl/post/problem-solving-with-lambda/


If you find bugs in the api, these are left in for you to try and solve to further your python and lambda skills.