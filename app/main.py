from typing import Optional
from fastapi import FastAPI

def gen(n):
    a = 0
    b = 1
    for i in range(1, n+1):
        a, b = b, a + b
    yield a

def fibonacci(n):
    for answer in gen(n):
        pass
    return answer

app = FastAPI()


@app.get("/api/v1/fibonacci")
def read_item(n: Optional[int] = 1):
    return fibonacci(n)


