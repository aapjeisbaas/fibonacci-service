from functools import lru_cache
from datetime import datetime
import random
import json

def gen(n):
    a = 0
    b = 1
    for i in range(1, n+1):
        a, b = b, a + b
    yield a

def fibonacci(n):
    for answer in gen(n):
        pass
    return answer

def range_test(nMax):
    for i in range(10):
      start = datetime.now()
      n = random.randrange(nMax)
      fibResult = fibonacci(n)
      print( str(n) + "\t" + str(datetime.now() - start))

def lambda_handler(event, context):
    #print("Received event: " + json.dumps(event, indent=2))
    requested_n = int(event['queryStringParameters']['n'])
    if requested_n > 25000:
      return "fib(25000) is the largest we compute on demand, if you need larger please contact sales through: https://aapjeisbaas.nl/#contact"
    else:
      return fibonacci(requested_n)


if __name__ == "__main__":
    # User input handling
    import argparse
    parser = argparse.ArgumentParser(description="Returns the n'th fibonacci number.")
    parser.add_argument('-n', required=True, type=int, default=10, help="which number in the fibonacci sequence you want")
    args = parser.parse_args()

    # execute calculation directly
    # print(fibonacci(args.n))

    # This represents an api gateway request to: https://sub.domain.tld/api/v1/fibonacci?n=100
    test_event = json.loads("""{
      "path": "/api/v1/fibonacci",
      "httpMethod": "GET",
      "queryStringParameters": {
        "n": "100"
      },
      "pathParameters": {
        "proxy": "/fibonacci"
      },
      "requestContext": {
        "identity": {
          "sourceIp": "127.0.0.1",
          "userAgent": "Custom User Agent String"
        },
        "path": "/prod/fibonacci",
        "protocol": "HTTP/1.1"
      }
    }""")

    # insert aur desired n in the test_event
    test_event['queryStringParameters']['n'] = args.n
    # execute calculation with test event through lambda_handler function
    print(lambda_handler(test_event, ''))
